let number = prompt("Enter a number", "0")
let factorial = 1;

function calcFactorial() {
    if (number == 0) {
        return 0
    } else if (Number.isInteger(+number)) {
        for (i = 1; i <= number; i++) {
            factorial *= i
        }
        return factorial
    } else {
        return "Error"
    }
}
document.write("Factorial = ", calcFactorial())