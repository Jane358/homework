start: while (true) {
    let userNumber1 = prompt('Введите первое целое число', '2')
    if (userNumber1 > 1 && Number.isInteger(+userNumber1)) {
        let userNumber2 = prompt('Введите второе целое число', '2')
        if (userNumber2 > 1 && Number.isInteger(+userNumber2)) {
            let biggerNumber = (+userNumber1 < +userNumber2) ? Number(userNumber2) : Number(userNumber1)
            let smallerNumber = (+userNumber1 < +userNumber2) ? Number(userNumber1) : Number(userNumber2)

            outer: for (smallerNumber; smallerNumber <= biggerNumber; smallerNumber++) {
                for (let j = 2; j < smallerNumber; j++) {
                    if ((smallerNumber % 2 !== 0) && (smallerNumber % j !== 0)) {
                        continue
                    } else {
                        if (smallerNumber == biggerNumber) {
                            break start
                        } else {
                            continue outer
                        }
                    }
                }
                console.log(smallerNumber)
                if (smallerNumber == biggerNumber) {
                    break start
                }
            }
        } else if (userNumber2 == null) {
            break start
        }
    } else if (userNumber1 == null) {
        break start
    }
}