start: while (true) {
    let userNumber = prompt('Введите целое число', '2')
    if (userNumber > 1 && Number.isInteger(+userNumber)) {
        outer: for (let i = 2; i <= userNumber; i++) {
            for (let j = 2; j < i; j++) {
                if ((i % 2 !== 0) && (i % j !== 0)) {
                    continue
                } else {
                    if (i == userNumber) {
                        break start
                    } else {
                        continue outer
                    }
                }
            }
            console.log(i)
            if (i == userNumber) {
                break start
            }
        }
    }
    else if (userNumber == null) {
        break
    }
}