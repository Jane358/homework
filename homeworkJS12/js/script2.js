let button = document.getElementById('button');

button.addEventListener('click', function () {
	this.remove();
	addInputs();
});

function addInputs(argument) {
	let circleDiameter = document.createElement('input');
	circleDiameter.setAttribute('type', 'text');
	circleDiameter.setAttribute('placeholder', 'Введите диаметр');
	let paint = document.createElement('button');
	paint.innerHTML = 'Нарисовать круг';
	paint.id = "paint";
	document.body.appendChild(circleDiameter);
	document.body.appendChild(paint);
	let wrapper = document.createElement('div');
	wrapper.id = 'circles-wrapper';
	document.body.appendChild(wrapper);
	paint.addEventListener('click', function () {
		let diameter = circleDiameter.value;
		for (let i = 0; i < 100; i++) {
			addCircle(diameter);
		}
		document.addEventListener('click', function (event) {
			if (event.target.className === 'circle') {
				event.target.remove();
			}
		});
	});
};

function addCircle(circleDiameter) {
	let wrapper = document.getElementById('circles-wrapper');
	let circle = document.createElement('div');
	circle.classList = 'circle';
	circle.style.width = circleDiameter+'px';
	circle.style.height = circleDiameter+'px';
	circle.style.background = getRandomColor();
	wrapper.appendChild(circle);

}

function getRandomColor() {
  let letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

