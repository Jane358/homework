$(document).ready(function() {

	$('#button').on('click',  function() {
		this.remove();
		addInputs();
	});

	$(document).on('click', '#paint', function() {
		let diameter = $('[name="diameter"]').val();
		for (let i = 0; i < 100; i++) {
			addCircle(diameter);
		}
	});

	$(document).on('click', '.circle', function(event) {
		$(this).remove()
	});

});

function addInputs(argument) {
	$('body').append('<input type="text" name="diameter" placeholder="Введите диаметр"><button id="paint">Нарисовать круг</button><div id="circles-wrapper"></div>');
};

function addCircle(circleDiameter) {
	let wrapper = $('#circles-wrapper');
	wrapper.append('<div class="circle" style="width:'+circleDiameter+'px;height:'+circleDiameter+'px;background:'+getRandomColor()+';"></div>');
}

function getRandomColor() {
  let letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

