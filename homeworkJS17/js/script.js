$(document).ready(function() {
	$('#button').on('click', function() {
		$(this).remove();
		addInputs();
	});

	$(document).on('click', '#paint', function () {
		let diameter = $('[name="diameter"]').val();
		let colour = $('[name="colour"]').val();
		console.log(diameter, colour);
		addCircle(diameter, colour);
	});
});

function addInputs() {
	$('body').append('<input type="text" name="diameter" placeholder="Введите диаметр"><input type="text" name="colour" placeholder="Введите цвет круга"><button id="paint">Нарисовать</button>');
};

function addCircle(circleDiameter, circleColour) {
	$('body').append('<div style="width:'+circleDiameter+'px;height:'+circleDiameter+'px;background:'+circleColour+';"></div>');
};