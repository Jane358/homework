const users = [{
  name: "Ivan",
  surname: "Ivanov",
  gender: "male",
  age: 30
},
{
  name: "Anna",
  surname: "Ivanova",
  gender: "female",
  age: 22
},
{
  name: "Janna",
  surname: "Ivanova",
  gender: "female",
  age: 22
}
];

const admins = [{
  name: "Anna",
  surname: "Ivanova",
  gender: "female",
  age: 22
},
{
  name: "Ivan",
  surname: "Ivanov",
  gender: "male",
  age: 30
}];

function excludeBy(peopleList, excluded, argument) {
	let resultArray = [];
	peopleList.forEach(function(item, i, peopleList) {
		let count = 1;
		excluded.forEach(function(excluded_item, j, excluded) {
			if(item[argument] != excluded_item[argument]) {
				count = count * 1;
			} else {
				count = 0;
			}
		});
		if (count == 1) {
			resultArray.push(item);
		}
	});
	return resultArray;
};

console.log(excludeBy(users, admins, "name"));